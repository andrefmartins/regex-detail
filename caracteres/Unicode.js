// no início...
// um byte (8 bits) - 255 caracteres
// símbolos, pontuação, A-Z, a-z, 0-9

// dois bytes (16 bits) - 65500+ caracteres
// +símbolos, +pontuação, A-Z, a-z, 0-9

// Unicode
// quantidade de bytes variável - Expansível
// Suporta mais de 1 Milhão de caracteres
// atualmente tem mais de 100.000 caracteres atribuidos

// https://unicode-table.com/pt