const texto = '.$+*?-';

console.log(texto.match(/[+.?*$]/g)); // não precisa de escape dentro do conjunto(a maioria
console.log(texto.match(/[$-?]/g)); // isso é um intervalo(range)

//  NAO é um intervalo(range)
console.log(texto.match(/[$\-?]/g));
console.log(texto.match(/[-?]/g));

// pode nao precisar de scape dentro do conjunto: - [ ] ^