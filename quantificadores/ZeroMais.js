const texto1 = 'De longe eu avistei o fogo e uma pessoa gritando: FOGOOOOOO!';
const texto2 = 'There is a big fog in NYC';

// * -> zero ou mais
const regex = /fogo*/gi; // quantificador guloso(captura o máximo possível)
const regex2 = /fogo*?/gi; // quando se adiciona o ? ele deixa de ser um quantificador guloso e pega somente 1
console.log(texto1.match(regex)); // aplicando o quantificador guloso
console.log(texto2.match(regex)); // aplicando o quantificador guloso
console.log(texto1.match(regex2)); 
console.log(texto2.match(regex2)); 
