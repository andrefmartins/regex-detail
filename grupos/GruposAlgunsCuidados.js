const texto = 'Pedrinho (filho do Pedro Silva) é doutor do ABC!';

console.log(texto.match(/[(abc)]/gi)); // não é um grupo, dentro de um conjunto, os grupos não existem
console.log(texto.match(/([abc])/gi));
console.log(texto.match(/(abc)/gi));

// **OBS** -> Tome cuidado ao usar grupos, tente resolver o seu problema, com uma regex mais simples possível!
