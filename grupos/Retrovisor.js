const texto1 = '<b>Destaque</b><strong>Forte</strong><div>Conteudo</div>';
console.log(texto1.match(/<(\w+)>.*<\/\1>/g)); // /1 -> referência o retrovisor do grupo1, ou seja, vai pegar a msm condição do grupo 1.

const texto2 = 'Lentamente é mente muito lenta.';
console.log(texto2.match(/(lenta)(mente).*\2.*\1/gi));
console.log(texto2.match(/(?:lenta)(mente).*\1/gi)); // ?: não guarda o valor

console.log(texto2.match(/(lenta)(mente)/gi));
console.log(texto2.match(/(lenta)(mente)?/gi));
console.log(texto2.replace(/(lenta)(mente)/gi, '$2')); // $2 referência o grupo 2 usando o replace


const texto3 = 'abcdefghijkll';
console.log(texto3.match(/(a)(b)(c)(d)(e)(f)(g)(h)(i)(j)(k)(l)\12/g)); // suporta uma quantidade de retrovisores maiores que 9
